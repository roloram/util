# -*- coding: utf-8 -*-
'''
    Module: Moneda
    Copyright (C) 2013  roloram 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
'''
from genero import Genero

class Moneda(object):
    '''
    classdocs
    '''
    
    nombre_singular = ''
    nombre_plural = ''
    genero = Genero.MASCULINO
    nombre_centesimos_plural = ''
    nombre_centesimos_singular = ''

    def __init__(self, nombre_singular, nombre_plural, nombre_centesimos_plural,  nombre_centesimos_singular, genero = Genero.MASCULINO):
        '''
        Constructor
        '''
        self.nombre_singular = nombre_singular
        self.nombre_plural = nombre_plural
        self.nombre_centesimos_singular = nombre_centesimos_singular
        self.nombre_centesimos_plural = nombre_centesimos_plural
        self.genero = genero
