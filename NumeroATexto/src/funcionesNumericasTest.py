# -*- coding: utf-8 -*-
''' 
    Copyright (C) 2013  roloram 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
'''

import unittest
from funcionesNumericas import FuncionesNumericas
from moneda import Moneda
from genero import Genero

class FuncionesNumericasTest(unittest.TestCase):
    

    
    def test_convertir_numero_a_texto_entrada_no_numerica(self):
        self.assertRaises(ValueError, FuncionesNumericas.convertir_numero_a_texto, self, 'entrada no numerica')

    def test_convertir_numero_a_texto_string_numerico(self):
        self.assertRaises(TypeError, FuncionesNumericas.convertir_numero_a_texto, self, '157')

    def test_convertir_numero_a_texto_0(self):
        self.assertEqual('cero', FuncionesNumericas.convertir_numero_a_texto(self, 0))

    def test_convertir_numero_a_texto_1(self):
        self.assertEqual('uno', FuncionesNumericas.convertir_numero_a_texto(self, 1))

    def test_convertir_numero_a_texto_10(self):
        self.assertEqual('diez', FuncionesNumericas.convertir_numero_a_texto(self, 10))

    def test_convertir_numero_a_texto_11(self):
        self.assertEqual('once', FuncionesNumericas.convertir_numero_a_texto(self, 11))

    def test_convertir_numero_a_texto_12(self):
        self.assertEqual('doce', FuncionesNumericas.convertir_numero_a_texto(self, 12))

    def test_convertir_numero_a_texto_16(self):
        self.assertEqual('dieciseis', FuncionesNumericas.convertir_numero_a_texto(self, 16))

    def test_convertir_numero_a_texto_20(self):
        self.assertEqual('veinte', FuncionesNumericas.convertir_numero_a_texto(self, 20))

    def convertir_numero_a_texto_21(self):
        self.assertEqual('veintiuno', FuncionesNumericas.convertir_numero_a_texto(self, 21))

    def test_convertir_numero_a_texto_30(self):
        self.assertEqual('treinta', FuncionesNumericas.convertir_numero_a_texto(self, 30))

    def test_convertir_numero_a_texto_35(self):
        self.assertEqual('treinta y cinco', FuncionesNumericas.convertir_numero_a_texto(self, 35))

    def test_convertir_numero_a_texto_99(self):
        self.assertEqual('noventa y nueve', FuncionesNumericas.convertir_numero_a_texto(self, 99))

    def test_convertir_numero_a_texto_100(self):
        self.assertEqual('cien', FuncionesNumericas.convertir_numero_a_texto(self, 100))

    def test_convertir_numero_a_texto_101(self):
        self.assertEqual('ciento uno', FuncionesNumericas.convertir_numero_a_texto(self, 101))

    def test_convertir_numero_a_texto_110(self):
        self.assertEqual('ciento diez', FuncionesNumericas.convertir_numero_a_texto(self, 110))

    def test_convertir_numero_a_texto_115(self):
        self.assertEqual('ciento quince', FuncionesNumericas.convertir_numero_a_texto(self, 115))

    def test_convertir_numero_a_texto_120(self):
        self.assertEqual('ciento veinte', FuncionesNumericas.convertir_numero_a_texto(self, 120))

    def test_convertir_numero_a_texto_125(self):
        self.assertEqual('ciento veinticinco', FuncionesNumericas.convertir_numero_a_texto(self, 125))

    def test_convertir_numero_a_texto_135(self):
        self.assertEqual('ciento treinta y cinco', FuncionesNumericas.convertir_numero_a_texto(self, 135))

    def test_convertir_numero_a_texto_200(self):
        self.assertEqual('doscientos', FuncionesNumericas.convertir_numero_a_texto(self, 200))

    def test_convertir_numero_a_texto_358(self):
        self.assertEqual('trescientos cincuenta y ocho', FuncionesNumericas.convertir_numero_a_texto(self, 358))

    def test_convertir_numero_a_texto_1000(self):
        self.assertEqual('mil', FuncionesNumericas.convertir_numero_a_texto(self, 1000))

    def test_convertir_numero_a_texto_1001(self):
        self.assertEqual('mil uno', FuncionesNumericas.convertir_numero_a_texto(self, 1001))

    def test_convertir_numero_a_texto_1010(self):
        self.assertEqual('mil diez', FuncionesNumericas.convertir_numero_a_texto(self, 1010))

    def test_convertir_numero_a_texto_1013(self):
        self.assertEqual('mil trece', FuncionesNumericas.convertir_numero_a_texto(self, 1013))

    def test_convertir_numero_a_texto_1259(self):
        self.assertEqual('mil doscientos cincuenta y nueve', FuncionesNumericas.convertir_numero_a_texto(self, 1259))

    def test_convertir_numero_a_texto_2000(self):
        self.assertEqual('dos mil', FuncionesNumericas.convertir_numero_a_texto(self, 2000))

    def test_convertir_numero_a_texto_2764(self):
        self.assertEqual('dos mil setecientos sesenta y cuatro', FuncionesNumericas.convertir_numero_a_texto(self, 2764))

    def test_convertir_numero_a_texto_21999(self):
        self.assertEqual('veintiun mil novecientos noventa y nueve', FuncionesNumericas.convertir_numero_a_texto(self, 21999))

    def test_convertir_numero_a_texto_131421(self):
        self.assertEqual('ciento treinta y un mil cuatrocientos veintiuno', FuncionesNumericas.convertir_numero_a_texto(self, 131421))

    def test_convertir_numero_a_texto_1_000_000(self):
        self.assertEqual('un millon', FuncionesNumericas.convertir_numero_a_texto(self, 1000000))

    def test_convertir_numero_a_texto_2_000_000(self):
        self.assertEqual('dos millones', FuncionesNumericas.convertir_numero_a_texto(self, 2000000))

    def test_convertir_numero_a_texto_11_758_931(self):
        self.assertEqual('once millones setecientos cincuenta y ocho mil novecientos treinta y uno', FuncionesNumericas.convertir_numero_a_texto(self, 11758931))

    def test_convertir_numero_a_texto_1_000_000_000_000(self):
        self.assertEqual('un billon', FuncionesNumericas.convertir_numero_a_texto(self, 1000000000000))

    def test_convertir_numero_a_texto_6_000_000_000_000(self):
        self.assertEqual('seis billones', FuncionesNumericas.convertir_numero_a_texto(self, 6000000000000))

    def test_convertir_numero_a_texto_2_342_005_430_122_333(self):
        self.assertEqual('dos mil trescientos cuarenta y dos billones cinco mil cuatrocientos treinta millones ciento veintidos mil trescientos treinta y tres', FuncionesNumericas.convertir_numero_a_texto(self, 2342005430122333))

    def test_convertir_numero_a_texto_menos5(self):
        self.assertEqual('menos cinco', FuncionesNumericas.convertir_numero_a_texto(self, -5))

    def test_convertir_numero_a_texto_0coma5(self):
        self.assertEqual('cero con cinco /100', FuncionesNumericas.convertir_numero_a_texto(self, 0.05))

    def test_convertir_numero_a_texto_1coma23(self):
        self.assertEqual('uno con veintitres /100', FuncionesNumericas.convertir_numero_a_texto(self, 1.23))

    def test_convertir_numero_a_texto_100coma0(self):
        self.assertEqual('cien', FuncionesNumericas.convertir_numero_a_texto(self, 100.0))

    def test_convertir_numero_a_texto_100coma99(self):
        self.assertEqual('cien con noventa y nueve /100', FuncionesNumericas.convertir_numero_a_texto(self, 100.99))

    def test_convertir_numero_a_texto_10coma4(self):
        self.assertEqual('diez con cuarenta /100', FuncionesNumericas.convertir_numero_a_texto(self, 10.4))

    def test_convertir_numero_a_texto_1000coma11(self):
        self.assertEqual('mil con once /100', FuncionesNumericas.convertir_numero_a_texto(self, 1000.11))

    def test_convertir_numero_a_texto_1_femenino(self):
        self.assertEqual('una', FuncionesNumericas.convertir_numero_a_texto(self, 1, Genero.FEMENINO))

    def test_convertir_numero_a_texto_1_masculino(self):
        self.assertEqual('un', FuncionesNumericas.convertir_numero_a_texto(self, 1, Genero.MASCULINO))

    def test_convertir_numero_a_texto_21_femenino(self):
        self.assertEqual('veintiuna', FuncionesNumericas.convertir_numero_a_texto(self, 21, Genero.FEMENINO))

    def test_convertir_numero_a_texto_21_masculino(self):
        self.assertEqual('veintiun', FuncionesNumericas.convertir_numero_a_texto(self, 21, Genero.MASCULINO))

    def test_convertir_numero_a_texto_50_femenino(self):
        self.assertEqual('cincuenta', FuncionesNumericas.convertir_numero_a_texto(self, 50, Genero.FEMENINO))

    def test_convertir_numero_a_texto_50_masculino(self):
        self.assertEqual('cincuenta', FuncionesNumericas.convertir_numero_a_texto(self, 50, Genero.MASCULINO))

    def test_convertir_valor_monetario_a_texto_1(self):
        moneda =  Moneda('peso', 'pesos', 'centésimos', 'centésimo')
        self.assertEqual('un peso', FuncionesNumericas.convertir_valor_monetario_a_texto(self, 1, moneda))

    def test_convertir_valor_monetario_a_texto_10(self):
        moneda =  Moneda('peso', 'pesos', 'centésimos', 'centésimo')
        self.assertEqual('diez pesos', FuncionesNumericas.convertir_valor_monetario_a_texto(self, 10, moneda))

    def test_convertir_valor_monetario_a_texto_1121_liras(self):
        moneda =  Moneda('libra', 'libras', 'centavo', 'centavos', Genero.FEMENINO)
        self.assertEqual('mil ciento veintiuna libras', FuncionesNumericas.convertir_valor_monetario_a_texto(self, 1121, moneda))

