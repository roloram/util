# -*- coding: utf-8 -*-
''' 
    Copyright (C) 2013  roloram 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
'''
from genero import Genero

class FuncionesNumericas(object):
    '''
    classdocs
    '''

    def __init__(self, params):
        '''
        Constructor
        '''

    @staticmethod
    def convertir_numero_a_texto(self, numero, genero = Genero.INDEFINIDO):
        enteros = int(numero)
        retorno = FuncionesNumericas.numero_a_texto(self, enteros, genero).strip()
        if numero - enteros > 0:
            decimales = int(round(numero - enteros, 2) * 100)
            retorno += ' con ' + FuncionesNumericas.numero_a_texto(self, decimales) + '/100'
        return retorno

    @staticmethod
    def convertir_valor_monetario_a_texto(self, valor, moneda):
        enteros = int(valor)
        retorno = FuncionesNumericas.numero_a_texto(self, enteros, moneda.genero)
        if enteros == 1:
            retorno += moneda.nombre_singular
        else:
            retorno += moneda.nombre_plural
            
        if valor - enteros > 0:
            decimales = int(round(valor - enteros, 2) * 100)
            retorno += ' con ' + FuncionesNumericas.numero_a_texto(self, decimales) 
            if decimales == 1:
                retorno += moneda.nombre_centesimos_singular
            else:
                retorno += moneda.nombre_centesimos_plural
            
        return retorno


    @staticmethod
    def numero_a_texto(self, numero, genero = Genero.INDEFINIDO):
        retorno = ''
        
        if numero < 0:
            retorno = 'menos '
            retorno += FuncionesNumericas.numero_a_texto(self, -numero, genero)

        retorno, numero = FuncionesNumericas.procesar_billones(self, numero, retorno)
        retorno, numero = FuncionesNumericas.procesar_millones(self, numero, retorno)
        retorno, numero = FuncionesNumericas.procesar_miles(self, numero, retorno)
        retorno, numero = FuncionesNumericas.procesar_centenas(self, numero, retorno)
        retorno, numero, digito_decenas = FuncionesNumericas.procesar_decenas(self, numero, retorno)
        retorno, numero = FuncionesNumericas.procesar_unidades(self, numero, retorno, digito_decenas, genero)

        return retorno


    @staticmethod
    def parcial(self, numero, divisor, genero = Genero.INDEFINIDO):
        parcial = int(numero // divisor)
        texto = FuncionesNumericas.numero_a_texto(self, parcial, genero)
        numero -= parcial * divisor
        return texto, numero, parcial
    
    @staticmethod
    def procesar_billones(self, numero, retorno):
        parcial = 0
        divisor = 1000000000000
        if numero // divisor > 0:
            retorno_parcial, numero, parcial = FuncionesNumericas.parcial(self, numero, divisor)
            if retorno_parcial.endswith('uno '):
                retorno_parcial = retorno_parcial[:-4] + 'un '
            if parcial == 1:
                retorno += retorno_parcial + 'billon '
            else:
                retorno += retorno_parcial + 'billones '
        return retorno, numero

    @staticmethod
    def procesar_millones(self, numero, retorno):
        parcial = 0
        divisor = 1000000
        if numero // divisor > 0:
            retorno_parcial, numero, parcial = FuncionesNumericas.parcial(self, numero, divisor)
            if retorno_parcial.endswith('uno '):
                retorno_parcial = retorno_parcial[:-4] + 'un '
            if parcial == 1:
                retorno += retorno_parcial + 'millon '
            else:
                retorno += retorno_parcial + 'millones '
        return retorno, numero

    @staticmethod
    def procesar_miles(self, numero, retorno):
        parcial = 0
        divisor = 1000
        if numero // divisor > 0:
            parcial = numero // divisor
            if parcial == 1:
                numero -= parcial * divisor
                retorno += 'mil '            
            elif parcial > 1:
                retorno_parcial, numero, parcial = FuncionesNumericas.parcial(self, numero, divisor)
                if retorno_parcial.endswith('uno '):
                    retorno_parcial = retorno_parcial[:-4] + 'un '
                retorno += retorno_parcial + 'mil '
        return retorno, numero

    @staticmethod
    def procesar_centenas(self, numero, retorno):
        divisor = 100
        digito_centenas = int(numero // divisor)
        if digito_centenas > 0:
            if digito_centenas > 1:
                centenas = ['', 'cien', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']            
                retorno += centenas[digito_centenas] + ' '
            else:
                if int(numero - (digito_centenas * divisor)) > 0:
                    retorno += 'ciento '
                else:
                    retorno += 'cien ' 
        
            numero -= digito_centenas * divisor
        return retorno, numero

    @staticmethod
    def procesar_decenas(self, numero, retorno):
        divisor = 10
        digito_decenas = int(numero // divisor)

        if digito_decenas > 0:

            if digito_decenas > 2:
                decenas = ['', 'diez', 'veinte', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
                retorno += decenas[digito_decenas] + ' '

            numero -= digito_decenas * divisor
        return retorno, numero, digito_decenas

    @staticmethod
    def procesar_unidades(self, numero, retorno, digito_decenas, genero = Genero.MASCULINO):
        digito_unidades = int(numero)
        primera_decena = ['diez', 'once', 'doce', 'trece', 'catorce', 'quince', 'dieciseis', 'diecisiete', 'dieciocho', 'diecinueve']
        if digito_decenas == 1 :
            retorno += primera_decena[digito_unidades] + ' ' 
        elif digito_decenas == 2:
            if digito_unidades == 0:
                retorno += 'veinte '
            else:
                retorno += 'veinti' + FuncionesNumericas.formatear_unidades(self, digito_unidades, genero) + ' '
        else: 
            if digito_unidades > 0:
                if digito_decenas > 0:
                    retorno += 'y '
                retorno += FuncionesNumericas.formatear_unidades(self, digito_unidades, genero) + ' '
            if digito_unidades == 0 and retorno == '':                
                retorno = FuncionesNumericas.formatear_unidades(self, digito_unidades, genero) + ' '
        
        numero -= digito_unidades
        return retorno, numero

    @staticmethod
    def formatear_unidades(self, digito_unidades, genero):
        retorno = ''
        unidades = ['cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']
        if digito_unidades == 1:
            if genero == Genero.FEMENINO:
                retorno = 'una'
            elif genero == Genero.MASCULINO:
                retorno = 'un'
            elif genero == Genero.INDEFINIDO:
                retorno = 'uno'
        else:
            retorno = unidades[digito_unidades]
        
        return retorno
    